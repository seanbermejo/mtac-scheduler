# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0017_auto_20150511_0159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='class',
            name='instructor',
            field=models.ManyToManyField(related_name='classes', to='profiler.Employee', blank=True),
        ),
    ]

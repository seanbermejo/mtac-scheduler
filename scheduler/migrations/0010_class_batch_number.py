# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0009_auto_20150504_0120'),
    ]

    operations = [
        migrations.AddField(
            model_name='class',
            name='batch_number',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
    ]

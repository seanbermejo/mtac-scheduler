# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0001_initial'),
        ('scheduler', '0005_class_event'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='class',
            name='event',
        ),
        migrations.AddField(
            model_name='class',
            name='event',
            field=models.ManyToManyField(default=None, related_name='classes', null=True, to='schedule.Event'),
        ),
    ]

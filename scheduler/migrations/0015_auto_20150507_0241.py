# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0014_class_instructor'),
    ]

    operations = [
        migrations.AlterField(
            model_name='class',
            name='instructor',
            field=models.ForeignKey(related_name='classes', blank=True, to='profiler.Employee', null=True),
        ),
    ]

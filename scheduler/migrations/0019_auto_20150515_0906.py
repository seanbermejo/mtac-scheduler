# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0008_module'),
        ('scheduler', '0018_auto_20150515_0826'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='class',
            name='instructor',
        ),
        migrations.AddField(
            model_name='class',
            name='instructor',
            field=models.ForeignKey(related_name='classes', default=1, blank=True, to='profiler.Employee'),
            preserve_default=False,
        ),
    ]

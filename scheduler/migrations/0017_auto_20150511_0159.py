# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0008_module'),
        ('scheduler', '0016_class_module'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='class',
            name='instructor',
        ),
        migrations.AddField(
            model_name='class',
            name='instructor',
            field=models.ManyToManyField(related_name='classes', null=True, to='profiler.Employee', blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0008_module'),
        ('scheduler', '0015_auto_20150507_0241'),
    ]

    operations = [
        migrations.AddField(
            model_name='class',
            name='module',
            field=models.ForeignKey(related_name='classes', blank=True, to='profiler.Module', null=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0012_auto_20150504_0133'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='class',
            name='instructor_override',
        ),
    ]

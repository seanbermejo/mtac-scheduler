# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0008_auto_20150504_0057'),
    ]

    operations = [
        migrations.AlterField(
            model_name='class',
            name='event',
            field=models.OneToOneField(related_name='get_class', to='schedule.Event'),
        ),
    ]

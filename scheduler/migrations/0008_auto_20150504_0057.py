# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0001_initial'),
        ('scheduler', '0007_auto_20150503_2127'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='class',
            name='event',
        ),
        migrations.AddField(
            model_name='class',
            name='event',
            field=models.ForeignKey(related_name='get_class', default=1, to='schedule.Event'),
            preserve_default=False,
        ),
    ]

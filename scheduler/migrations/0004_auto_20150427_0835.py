# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0003_auto_20150427_0834'),
    ]

    operations = [
        migrations.AlterField(
            model_name='class',
            name='instructor_override',
            field=models.ForeignKey(blank=True, to='profiler.Employee', null=True),
        ),
    ]

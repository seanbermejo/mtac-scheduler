# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0006_auto_20150506_2352'),
        ('scheduler', '0013_remove_class_instructor_override'),
    ]

    operations = [
        migrations.AddField(
            model_name='class',
            name='instructor',
            field=models.ForeignKey(blank=True, to='profiler.Employee', null=True),
        ),
    ]

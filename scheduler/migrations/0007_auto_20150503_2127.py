# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0006_auto_20150503_1836'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='class',
            name='date',
        ),
        migrations.AddField(
            model_name='class',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 3, 21, 26, 48, 670535, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='class',
            name='modified_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 3, 21, 26, 55, 674994, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='time',
            name='creation_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 3, 21, 27, 5, 101938, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='time',
            name='modified_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 3, 21, 27, 8, 726921, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='class',
            name='event',
            field=models.ManyToManyField(related_name='classes', to='schedule.Event'),
        ),
    ]

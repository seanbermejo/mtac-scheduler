# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0002_auto_20150427_0814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='class',
            name='instructor_override',
            field=models.ForeignKey(to='profiler.Employee', blank=True),
        ),
    ]

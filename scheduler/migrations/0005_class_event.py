# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0001_initial'),
        ('scheduler', '0004_auto_20150427_0835'),
    ]

    operations = [
        migrations.AddField(
            model_name='class',
            name='event',
            field=models.ForeignKey(default=None, to='schedule.Event', null=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0011_class_hours'),
    ]

    operations = [
        migrations.AlterField(
            model_name='class',
            name='hours',
            field=models.IntegerField(default=8, help_text='This represents the number of hours the                             Class in taught in a day.'),
        ),
    ]

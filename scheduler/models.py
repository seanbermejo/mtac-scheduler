import time

from schedule import models as s
from django.utils.translation import ugettext_lazy as _
from django.db import models
from profiler import models as pm
# Create your models here.

class BaseModel(models.Model):

    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class Time(BaseModel):
    """
    This represents a time table
    we create fixed time slots for a class 

    Time start=8:00am end=9:00am
    """
    start = models.TimeField()
    end = models.TimeField()

    class Meta:
        verbose_name = _('Time slot')
        verbose_name_plural = _('Time slots')

    def __unicode__(self):
        start = self.start.strftime("%H:%M %p")
        end = self.end.strftime("%H:%M %p")
        return " - ".join([start, end])

class ClassManager(models.Manager):

    def filter_class(self, *args, **kwargs):
        return self.filter(*args, **kwargs).select_related('course', 'module', 'event', 'instructor')

class Class(BaseModel):
    """
    This refers to a Class instance where  a course is given a time slot
    and date. There is also an instructor override field for cases where 
    an instructor can't teach on a specific time and date.
    """
    batch_number = models.CharField(max_length=50)
    course = models.ForeignKey(pm.Course)
    module = models.ForeignKey(pm.Module, blank=True, null=True,
        related_name='classes')
    instructor = models.ForeignKey(pm.Employee, blank=True, related_name='classes')
    event = models.OneToOneField(s.Event, related_name='get_class')
    # event = models.ManyToManyField(s.Event, related_name='classes')
    hours = models.IntegerField(default=8, 
                help_text=_("This represents the number of hours the \
                            Class in taught in a day."))
    time_slot = models.ForeignKey(Time)

    objects = ClassManager()

    class Meta:
        verbose_name = _('Class')
        verbose_name_plural = _('Classes')


    def __unicode__(self):
        # return self.batch_number
        return " - ".join([self.batch_number, unicode(self.course), self.instructor.get_name()])
        # return " | ".join([self.batch_number, unicode(self.course), unicode(self.instructor)])

    def faculty_load_format(self):
        return "{s.course} ({s.batch_number})".format(s=self)

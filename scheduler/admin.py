from django.contrib import admin
from . import models as m
from schedule import models as s

class ClassInline(admin.StackedInline):
	model = m.Class

class EventAdmin(admin.ModelAdmin):
	inlines = [
		ClassInline,
	]
	# exclude = ('event',)

# Register your models here.
admin.site.register(m.Time)
# admin.site.register(m.Class)
admin.site.unregister(s.Event)
admin.site.register(s.Event, EventAdmin)
# admin.site.register(s.Event, EventAdmin)
# admin.site.register(m.Class, ClassAdmin)

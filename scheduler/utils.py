from itertools import groupby


default_sentinel = object()

def getattrs(obj, attr, default=default_sentinel):
    """
    Like Python's builtin ``getattr`` except that it can traverse attributes

    Example usage::

        name = getattrs(job, 'owner.profile.name', '')

    >>> Obj = type('Obj', (object,), {'foo': 'FOO'})
    >>> obj = Obj()
    >>> obj.bar = obj
    >>> getattrs(obj, 'foo')
    'FOO'
    >>> getattrs(obj, 'ack', 'ACK')
    'ACK'
    >>> getattrs(obj, 'ack')
    Traceback (most recent call last):
        ...
    AttributeError: 'Obj' object has no attribute 'ack'
    >>> getattrs(obj, 'bar.foo')
    'FOO'
    >>> getattrs(obj, 'bar.ack', 'ACK')
    'ACK'
    >>> getattrs(obj, 'bar.ack')
    Traceback (most recent call last):
        ...
    AttributeError: 'Obj' object has no attribute 'ack'
    """
    try:
        return reduce(getattr, attr.split('.'), obj)
    except AttributeError:
        if default is default_sentinel:
            raise
        return default


def callattr(obj, attr_name, default=default_sentinel, args=None, kwargs=None):
    """
    Like Python's built-in ``getattr`` except that it calls the attr using the
    supplied ``args`` and ``kwargs``. Also, it can traverse attributes deeply
    since it uses ``getattrs``.

    Example usage::

        callattr(possible_datetime_obj, 'time', '')

    Note that the examples below are contrived for tests and not the kind of
    real world usage expected.

    >>> l = [1,2,3,4]
    >>> callattr(l, 'pop')
    4
    >>> callattr(l, 'foo')
    Traceback (most recent call last):
        ...
    AttributeError: 'list' object has no attribute 'foo'
    >>> callattr(l, 'foo', 'FOO')
    'FOO'
    >>> callattr(l, 'append', args=[4])
    >>> callattr(object, '__doc__')
    Traceback (most recent call last):
        ...
    TypeError: 'str' object is not callable
    >>> callattr(l, 'pop.ack')
    Traceback (most recent call last):
        ...
    AttributeError: 'builtin_function_or_method' object has no attribute 'ack'
    >>> callattr(l, 'pop.ack', 'ACK')
    'ACK'
    """
    args = args or []
    kwargs = kwargs or {}

    try:
        return getattrs(obj, attr_name)(*args, **kwargs)
    except AttributeError:
        if default is default_sentinel:
            raise
        return default

def groupby_date(events):
	return [(key, list(values)) for key, values in groupby(events, key=lambda occur: occur.start.date())]

def days_hours_minutes(td):
    return td.days, td.total_seconds() // 60 // 60, td.total_seconds() // 60
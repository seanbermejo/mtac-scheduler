from django import forms
from profiler import models as pm
from django.utils.translation import ugettext_lazy as _

class ReportFilterForm(forms.Form):
    start_date = forms.DateTimeField(required=False, widget=forms.HiddenInput())
    end_date = forms.DateTimeField(required=False, widget=forms.HiddenInput())
    batch_number = forms.CharField()
    course = forms.ModelChoiceField(queryset=pm.Course.objects.all())
    
    def __init__(self, *args, **kwargs):
    	super(ReportFilterForm, self).__init__(*args, **kwargs)
    	for key, field in self.fields.iteritems():
	    	field.widget.attrs = {'class': 'form-control'}

class FacultyLoadFilterForm(forms.Form):
    start_date = forms.DateTimeField(required=False, widget=forms.HiddenInput())
    end_date = forms.DateTimeField(required=False, widget=forms.HiddenInput())
    instructor = forms.ModelChoiceField(queryset=pm.Employee.objects.all())
    
    def __init__(self, *args, **kwargs):
        super(FacultyLoadFilterForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.iteritems():
            field.widget.attrs = {'class': 'form-control'}


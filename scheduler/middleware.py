
from django.utils.translation import activate


class ActivateLocale(object):
	
	def process_request(self, request):
		activate('en')
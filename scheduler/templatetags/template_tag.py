from datetime import time
from django import template

from ..utils import getattrs, days_hours_minutes
register = template.Library()

@register.filter
def remove_duplicate(iterable):
	return list(set(iterable))

@register.filter
def make_list(iterable, attr):
	return [getattrs(obj, attr, '') for obj in iterable]
	
@register.filter
def hours(start, end):
	"""
	Returns the difference in hours between two datetime objects
	"""
	days, hours, _ = days_hours_minutes(end - start)
	# this snippet is for removing the lunch period 
	# between dates
	if end.time() > time(12):
		if days > 1:
			hours -= 1 * days
		else:
			hours -= 1
	return hours

@register.filter
def minutes(start, end):
	"""
	Returns the difference in minutes between two datetime objects
	"""
	_, _, minutes = days_hours_minutes(end - start)
	return minutes

@register.filter
def days(start, end):
	"""
	Returns the difference in days between two datetime objects
	"""
	days, _, _ = days_hours_minutes(end - start)
	return days 


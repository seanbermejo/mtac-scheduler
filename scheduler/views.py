import json
import pytz
import datetime

from django.utils.timezone import utc
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, Http404
from django.views.generic import View
from django.shortcuts import render

from schedule.models import Calendar, Occurrence, Event
from .forms import ReportFilterForm, FacultyLoadFilterForm
from .templatetags.template_tag import hours
from .models import Class
from .utils import groupby_date


# Create your views here.
class ReportFilterView(View):
    form_class = ReportFilterForm
    template_name = 'report_filter.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # <process form cleaned data>
            start = form.cleaned_data['start_date']
            end = form.cleaned_data['end_date']
            events = []
            for _class in Class.objects.filter(batch_number=form.cleaned_data['batch_number'],
                                               course_id=form.cleaned_data['course'],
                                               event__start__gte=start,
                                               event__end__lte=end).order_by('event__start')\
                                               .select_related('event'):
                events.extend(_class.event.get_occurrences(start, end))
            events = groupby_date(events)
            start = start.strftime('%m/%d/%Y')
            end = end.strftime('%m/%d/%Y')
            return render(request, self.template_name, {
                     'form': form, 
                     'events': events,
                     'start_date': start,
                     'end_date': end,
                     'batch_number': form.cleaned_data['batch_number'],
                     'course': form.cleaned_data['course'].id
            })

        return render(request, self.template_name, {'form': form})

class FacultyLoadFilterView(View):
    form_class = FacultyLoadFilterForm
    template_name = 'faculty_load_filter.html'
    report_template_name = 'faculty_report.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # <process form cleaned data>
            start = form.cleaned_data['start_date']
            end = form.cleaned_data['end_date']
            events_list = []
            events = Event.objects.filter(
                        Q(end_recurring_period__gte=start) | Q(end_recurring_period__lte=end),
                        Q(start__gte=start) | Q(end__lte=end),
                        get_class__instructor_id=form.cleaned_data['instructor'],
                        ).order_by('start').select_related('get_class')
            for event in events:
                events_list.extend(event.get_occurrences(start, end))
            events_list.sort(key=lambda e: e.start)
            # get the total number of hours excluding lunch time in 
            total_hours = sum((hours(e.start, e.end) for e in events_list))
            start = start.strftime('%m/%d/%Y')
            end = end.strftime('%m/%d/%Y')
            ctx = {
             'form': form, 
             'occurrences': events_list,
             'start_date': start,
             'end_date': end,
             'total_hours': total_hours,
             'instructor': form.cleaned_data['instructor']
            }
            if request.POST.get('report'):
                return render(request, self.report_template_name, ctx)
            return render(request, self.template_name, ctx)

        return render(request, self.template_name, {'form': form})

def report(request):
    form = ReportFilterForm(request.POST)
    if form.is_valid():
        # <process form cleaned data>
        start = form.cleaned_data['start_date']
        end = form.cleaned_data['end_date']
        events = []
        for _class in Class.objects.filter(batch_number=form.cleaned_data['batch_number'],
                                           course_id=form.cleaned_data['course'],
                                           event__start__gte=start,
                                           event__end__lte=end).order_by('event__start')\
                                           .select_related('event'):
            events.extend(_class.event.get_occurrences(start, end))
        events = groupby_date(events)
        start = start.strftime('%m/%d/%Y')
        end = end.strftime('%m/%d/%Y')
        return render(request, 'basic_training.html', {
                 'events': events,
                 'start_date': start,
                 'end_date': end,
                 'batch_number': form.cleaned_data['batch_number'],
                 'course': form.cleaned_data['course'].id
        })
    else:
        raise Http404



def api_occurrences(request):
    '''
    this is the same as with schedule.views.api_occurrences
    overrided urls to point to this view
    the only difference on this is the title
    '''
    utc=pytz.UTC
    from django.db import connection
    print 'Start ' + str(len(connection.queries))
    # version 2 of full calendar
    if '-' in request.GET.get('start'):
        convert = lambda d: datetime.datetime.strptime(d, '%Y-%m-%d')
    else:
        convert = lambda d: datetime.datetime.utcfromtimestamp(float(d))
    start = utc.localize(convert(request.GET.get('start')))
    end = utc.localize(convert(request.GET.get('end')))
    calendar = get_object_or_404(Calendar, slug=request.GET.get('calendar_slug'))
    response_data =[]
    for event in calendar.events.filter(start__gte=start, end__lte=end).select_related('get_class', 'get_class__course', 'get_class__module').prefetch_related('get_class__instructor'):
        occurrences = event.get_occurrences(start, end)
        for occurrence in occurrences:
            # http://fullcalendar.io/docs/event_data/Event_Object/
            response_data.append({
                "id": occurrence.id,
                "title": unicode(occurrence.event.get_class),
                "start": occurrence.start.isoformat(),
                "end": occurrence.end.isoformat(),
                "url": reverse('admin:schedule_event_change', args=[occurrence.event.id])

            })
    print 'End ' + str(len(connection.queries))
    return HttpResponse(json.dumps(response_data), content_type="application/json")

from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from views import (
	ReportFilterView,
	FacultyLoadFilterView,
	report
)
urlpatterns = patterns('',
    url(r'^fullcalendar/', TemplateView.as_view(template_name="fullcalendar.html"), name='fullcalendar'),
    url(r'^api/occurrences', 'scheduler.views.api_occurrences', name='api_occurences'),
    url(r'^report$', report, name='report'),
    url(r'^faculty-load/report/$', TemplateView.as_view(template_name='faculty_report.html'), name='faculty-load-report'),
	url(r'^report/filter/$', ReportFilterView.as_view(), name='report-filter'),
	url(r'^faculty-load/filter/$', FacultyLoadFilterView.as_view(), name='faculty-load-filter'),
)
from django.contrib import admin
from . import models as m
# Register your models here.


class CoursesInline(admin.TabularInline):
	model = m.Employee.approved_courses.through

	# todo
	class Meta:
		verbose_name = 'Approved Course'
		verbose_name_plural = 'Approved Courses'

class EmployeeAdmin(admin.ModelAdmin):
	inlines = [
		CoursesInline
	]
	exclude = ('approved_courses',)

admin.site.register(m.Employee, EmployeeAdmin)
admin.site.register(m.Rank)
admin.site.register(m.Course)
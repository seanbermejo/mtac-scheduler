# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0003_auto_20150427_0833'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='employee',
            name='age',
        ),
        migrations.AlterField(
            model_name='employee',
            name='sex',
            field=models.CharField(max_length=10),
        ),
    ]

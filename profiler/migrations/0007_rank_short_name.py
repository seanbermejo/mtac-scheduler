# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0006_auto_20150506_2352'),
    ]

    operations = [
        migrations.AddField(
            model_name='rank',
            name='short_name',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
    ]

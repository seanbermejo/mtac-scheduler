# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0010_auto_20150518_1221'),
    ]

    operations = [
        migrations.AddField(
            model_name='rank',
            name='description',
            field=models.TextField(default=b''),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='nationality',
            field=models.CharField(max_length=200, choices=[(b'Filipino', b'Filipino')]),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0008_module'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='nickname',
            field=models.CharField(default='', max_length=10),
            preserve_default=False,
        ),
    ]

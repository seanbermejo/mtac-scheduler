# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0009_employee_nickname'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='course_type',
            field=models.IntegerField(default=1, choices=[(1, b'Basic'), (2, b'Advanced')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='course',
            name='name',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='course',
            name='short_name',
            field=models.CharField(max_length=20),
        ),
    ]

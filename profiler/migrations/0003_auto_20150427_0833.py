# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0002_auto_20150427_0814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='address',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='employee',
            name='rate_override',
            field=models.FloatField(blank=True),
        ),
    ]

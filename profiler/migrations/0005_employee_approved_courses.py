# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiler', '0004_auto_20150503_2332'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='approved_courses',
            field=models.ManyToManyField(to='profiler.Course'),
        ),
    ]

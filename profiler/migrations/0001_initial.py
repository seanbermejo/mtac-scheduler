# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_name', models.CharField(max_length=10)),
                ('name', models.CharField(max_length=50)),
                ('repeat', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('middle_name', models.CharField(max_length=200)),
                ('age', models.IntegerField()),
                ('birthdate', models.DateField()),
                ('sex', models.CharField(max_length=200)),
                ('nationality', models.CharField(max_length=200)),
                ('accredited_training', models.BooleanField()),
                ('employee_type', models.IntegerField(choices=[(0, b'Instructor'), (1, b'Assessor'), (2, b'Supervisor')])),
                ('years_on_board', models.IntegerField()),
                ('rate_override', models.FloatField()),
                ('address', models.CharField(max_length=500)),
                ('contact_number', models.CharField(max_length=100)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Rank',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('rate', models.FloatField()),
            ],
        ),
        migrations.AddField(
            model_name='employee',
            name='rank',
            field=models.ForeignKey(to='profiler.Rank'),
        ),
        migrations.AddField(
            model_name='course',
            name='instructor',
            field=models.ForeignKey(to='profiler.Employee'),
        ),
    ]

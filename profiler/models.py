
from django.db import models
# Create your models here.


NATIONALITY_CHOIES = [
    ('Filipino', 'Filipino'),
]
class Person(models.Model):
    """
    An abstract class for Employee to contain
    personal information of a Person
    """
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    middle_name = models.CharField(max_length=200)
    nickname = models.CharField(max_length=10)
    birthdate = models.DateField()
    sex = models.CharField(max_length=10)
    nationality = models.CharField(max_length=200, choices=NATIONALITY_CHOIES)

    def __unicode__(self):
        return u', '.join([self.last_name, self.first_name])
    
    class Meta:
        abstract = True

class Rank(models.Model):
    """
    Rank refers to a position of the Employee on 
    the organization/company

    Use Case
    1. If years on board < 1; not 310 per hour

    >>> rank = Rank(title='Captain', rate=310)
    >>> rank = Rank(title='Chief Engineer', rate=310)
    >>> rank = Rank(title='Chief Mate', rate=260)
    >>> rank = Rank(title='OICNW', rate=220)
    >>> rank = Rank(title='OICEW', rate=220)
    >>> rank = Rank(title='Nurse', rate=180)
    """

    title = models.CharField(max_length=50)
    short_name = models.CharField(max_length=50)
    rate = models.FloatField()
    description = models.TextField(default='')

    def __unicode__(self):
        if self.short_name:
            return self.short_name
        return self.title 

class Employee(Person):
    """
    Employee refers to the Person who works for the 
    organization/company
    """
    TYPE_CHOICES = [
        (0, 'Instructor'),
        (1, 'Assessor'),
        (2, 'Supervisor'),
    ]
    # this refers to an Employee which has passed the 
    # training required for an Employee to teach.
    accredited_training = models.BooleanField()
    # this refers to an Instructor, Assessor and Supervisor
    employee_type = models.IntegerField(choices=TYPE_CHOICES)
    rank = models.ForeignKey(Rank)
    years_on_board = models.IntegerField()
    rate_override = models.FloatField(blank=True)
    address = models.TextField()
    contact_number = models.CharField(max_length=100)
    approved_courses = models.ManyToManyField('Course')

    def __unicode__(self):
        return u' '.join([unicode(self.rank), super(Employee, self).__unicode__()])

    @property
    def rate(self):
        if self.rate_override:
            return self.ra23te_override
        return self.rank.rate

    def get_name(self):
        if self.nickname:
            return self.nickname
        return self.last_name
    
class Course(models.Model):
    """
    Course is meant for trainings to be taught 
    by the Employee of the organization/company

    Data such as 

    Basic Training Course - BT-PST, BT-FPFF, BT-EFA, BT-PSSR(2days), Actual Demo, BT Assessment, EFA Demo & Assessment
    SAT - SDSD (1 day)
    Consolidated Marpol I-VI ( 5 days)
    PSCRB (4 days) - first 3 days(same instructor), 4th days 
    BTOCTCO (5 days)
    Refresher Training (2 days)
    Updating Training (1 day)
    AFF (5 days)
    MEFA (3 days)
    Deck Watchkeeping (4 days)
    Engine Watchkeeping (4 days)

    """
    TYPE_CHOICES = [
        (1, 'Basic'),
        (2, 'Advanced'),
    ]
    short_name = models.CharField(max_length=20)
    name = models.CharField(max_length=200)
    course_type = models.IntegerField(choices=TYPE_CHOICES)

    def __unicode__(self):
        # return u' - '.join([self.short_name, self.name])
        return self.short_name

class Module(models.Model):
    """
    Module means subject taught in the Course
    """
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name
    
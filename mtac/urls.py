from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.contrib import admin
from scheduler.views import *

urlpatterns = patterns('',
    # Examples:
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^scheduler/', include('scheduler.urls')),
    url(r'^schedule/', include('schedule.urls')),
    # override api call to the view in scheduler.api_occurrences
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


# if settings.DEBUG:
#     urlpatterns += patterns('',
#         (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
#         (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
#     )

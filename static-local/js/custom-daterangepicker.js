// custom date range picker
// modify this range picker and it will change on all 
function get_date(string){
  var date = string.split('/');
  date = new Date(date[2], date[0] - 1, date[1]);
  return date;
}
$(document).ready(function(){

    var el_start_date = $("input#id_start_date[type=hidden]");
    if(el_start_date.val() !== "") temp_start_date = String(el_start_date.val());
    else temp_start_date = 'None';
    
    var el_end_date = $("input#id_end_date[type=hidden]");
    if(el_end_date.val() !== "") temp_end_date = String(el_end_date.val());
    else temp_end_date = 'None';
    if (temp_start_date == 'None' || temp_end_date == 'None'){
      $('#reportrange').daterangepicker(
        {
          format: 'MM/DD/YYYY',
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
           // startDate: moment().subtract(6, 'days'),
           startDate: moment(),
           endDate: moment()
        },
        function(start, end) {
            $('#reportrange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            el_start_date.val(start.format('MM/DD/YYYY'));
            el_end_date.val(end.format('MM/DD/YYYY'));
        }
        );
    }
    else{
        start_date = get_date(temp_start_date);
        end_date = get_date(temp_end_date);
        $('#reportrange').daterangepicker(
        {
          format: 'MM/DD/YYYY',
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
           startDate: start_date,
           endDate: end_date
        },
        function(start, end) {
            $('#reportrange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            el_start_date.val(start.format('MM/DD/YYYY'));
            el_end_date.val(end.format('MM/DD/YYYY'));
        }
        );
    }

  // $('#reportrange').click(function() {
  //   $.scrollTo($(this), 800);
  // });
});